import {UserRepository} from '../../../infrastructure/driven-adapters/typeorm/repositories/user/user.repository';
import{UserUseCase} from "../../../application/use-cases/user/user.use-case";
import {getDbConnection} from "../../../infrastructure/driven-adapters/typeorm/config/db.config";
import {UserController} from "../../../presentation/controllers/user/user.controller";
import {
    GeneralResponseHandler
} from "@smartsoft/backend-utils/dist/adapters/responses/general-response-handler.adapter";

let userRepository: UserRepository;
let userUseCase: UserUseCase;
let userController: UserController

export const userFactory = () => {
    if(!userRepository){
        userRepository = new UserRepository(getDbConnection());
    }
    if(!userUseCase){
        userUseCase = new UserUseCase(userRepository);
    }
    if(!userController){
        userController = new UserController(userUseCase, new GeneralResponseHandler());
    }
    return {
        userRepository,
        userUseCase,
        userController
    }
};