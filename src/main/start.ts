import {connectToDb} from "../infrastructure/driven-adapters/typeorm/config/db.config";
import {startApp} from "../infrastructure/entry-points/express/setup/server";

export async function startApplication(preventListen = false){
    await connectToDb();
    const expressApp = startApp(preventListen);
    return {
        express: expressApp
    }
}