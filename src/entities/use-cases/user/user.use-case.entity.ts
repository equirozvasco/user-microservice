import {UserCreateInput, UserSearchInput, UserUpdateInput, UserSearchCreditInput} from '../../schemas/types/user';
import {User, UserCreditInfo} from '../../models/user/user.model.entity';

export interface IUserUseCase {
    create(input: UserCreateInput): Promise<User>;
    create(input: UserCreateInput, transactionManager: any): Promise<User>;
    update(input: UserUpdateInput): Promise<User>;
    update(input: UserUpdateInput, transactionManager: any): Promise<User>;
    delete(id: number): Promise<void>;
    delete(id: number, transactionManager: any): Promise<void>;
    getById(input: Omit<UserSearchInput, "searchBy"> & {searchBy: {id: number}}): Promise<User | undefined>;
    getById(input: Omit<UserSearchInput, "searchBy"> & {searchBy: {id: number}}, transactionManager: any): Promise<User | undefined>;
    getOneBy(input: UserSearchInput): Promise<User | undefined>;
    getManyBy(input: UserSearchInput): Promise<Array<User>>;
    validateCredit(input: UserSearchCreditInput): Promise<UserCreditInfo>
    validateCredit(input: UserSearchCreditInput, transactionManager: any): Promise<UserCreditInfo>
}