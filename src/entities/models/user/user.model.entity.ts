export interface User{
    id: number;
    firstName: string;
    secondName: string | null;
    lastName: string | null;
    secondLastName: string | null;
    email: string;
    rol: string;
    credit: number;
    active: boolean;
    createAt: Date;
    updateAt: Date;
    deleteAt: Date | null;
}
export interface UserCreditInfo{
    currentCredit: number;
    active: boolean;
}