import Joi from 'joi';

export const UserGeneralOptionsInputSchema = Joi.object({

}).meta({className: "UserGeneralOptionsInput"});

export const UserCreateInputSchema = Joi.object({
    data: Joi.object({
        firstName: Joi.string().required(),
        secondName: Joi.string().optional(),
        lastName: Joi.string().optional(),
        secondLastName: Joi.string().optional(),
        email: Joi.string().email().required(),
        rol: Joi.string().required(),
        credit: Joi.number().required()
    }).required().min(1),
    options: UserGeneralOptionsInputSchema.optional(),
}).meta({className: 'UserCreateInput'});

export const UserUpdateInputSchema = Joi.object({
    searchBy: Joi.object({
        id: Joi.number().required()
    }).required().min(1),
    data: Joi.object({
        firstName: Joi.string().optional(),
        secondName: Joi.string().optional(),
        lastName: Joi.string().optional(),
        secondLastName: Joi.string().optional(),
        email: Joi.string().email().optional(),
        rol: Joi.string().optional(),
        credit: Joi.number().optional()
    }).required().min(1),
    options: UserGeneralOptionsInputSchema.optional(),
}).meta({className: 'UserUpdateInput'});

export const UserSearchCreditInputSchema = Joi.object({
    searchBy: Joi.object({
        id: Joi.number().required()
    }).required().min(1),
    data: Joi.object({
        credit: Joi.number().required()
    }).required().min(1),
    options: UserGeneralOptionsInputSchema.optional(),
}).meta({className: 'UserSearchCreditInput'});

export const UserDeleteInputSchema = Joi.object({
    searchBy: Joi.object({
        id: Joi.number().required()
    }).required(),
}).meta({className: "UserDeleteInput"});

export const UserSearchInputSchema = Joi.object({
    searchBy: Joi.object({
        id: Joi.number().optional(),
        firstName: Joi.string().optional(),
        lastName: Joi.string().optional(),
        secondLastName: Joi.string().optional(),
        email: Joi.string().email().optional(),
    }),
    options: UserGeneralOptionsInputSchema.optional()
}).meta({className: 'UserSearchInput'});
