import {ControllerAction, IController} from "@smartsoft/backend-utils/dist/ports/controllers/controller.port";

import {
    ResponseHandler,
    ResponseModel,
    ResponseStatusCode
} from "@smartsoft/backend-utils/dist/ports/responses/response.port";
import {User, UserCreditInfo} from "../../../entities/models/user/user.model.entity";
import {
    UserSearchCreditInput,
    UserSearchInput,
    UserDeleteInput,
    UserUpdateInput,
    UserCreateInput
} from '../../../entities/schemas/types/user'
import {UserUseCase} from "../../../application/use-cases/user/user.use-case";
import {KnownApplicationErrors, StatusErrorCode} from "@smartsoft/backend-utils/dist/entities/errors/errors.entity";
import {ApplicationError} from "@smartsoft/backend-utils/dist/errors/application-error.helper";
import {validateBody} from "../../../common/helpers/controllers/basicValidations";
import {RequestModel} from "@smartsoft/backend-utils/dist/ports/requests/request.port";
import {UserSearchCreditInputSchema} from "../../../entities/schemas/definitions/user/user.schema.entity";

export type UserRequestCreateInput = UserCreateInput;
export type UserRequestUpdateInput = UserUpdateInput;
export type UserRequestDeleteInput = UserDeleteInput;
export type UserRequestSearchCreditInput = UserSearchCreditInput;
export type UserRequestSearchInput = UserSearchInput["searchBy"];
export type UserRequestGeneralParamsInput = {
    id?: number;
};

export class UserController implements IController{
    [name: string]: ControllerAction | unknown;
    public static instance: UserController;
    constructor(
        private readonly userUseCase: UserUseCase,
        private readonly responseHandler: ResponseHandler
    ) {
        UserController.instance = this;
    }

    public async create(request: RequestModel<UserRequestCreateInput, UserRequestGeneralParamsInput>): Promise<ResponseModel<User>>{
        const validatedRequest = validateBody(request);
        const inputData: UserCreateInput = {
            ...validatedRequest.body
        };
        const user = await UserController.instance.userUseCase.create(inputData);
        return UserController.instance.responseHandler.response("success", ResponseStatusCode.Created, user);
    }

    public async searchUserCredit(request: RequestModel<UserRequestSearchCreditInput, UserRequestGeneralParamsInput>): Promise<ResponseModel<UserCreditInfo>>{
        const validateRequest = validateBody(request);
        const inputData: UserSearchCreditInput = {
            ...validateRequest.body
        };
        const credit = await UserController.instance.userUseCase.validateCredit(inputData);
        return UserController.instance.responseHandler.response("success", ResponseStatusCode.OK, credit);
    }

    public async update(request: RequestModel<UserRequestUpdateInput, UserRequestGeneralParamsInput>): Promise<ResponseModel<User>>{
        const validatedRequest = validateBody(request);
        if(request.params?.id === undefined){
            throw new ApplicationError("no id was provided", KnownApplicationErrors.ID_NOT_PROVIDED, StatusErrorCode.BadRequest);
        }
        const inputData: UserUpdateInput = {
            ...validatedRequest.body,
            searchBy: {
                id: parseInt(request.params.id + '')
            }
        };
        const user = await UserController.instance.userUseCase.update(inputData);
        return UserController.instance.responseHandler.response("success", ResponseStatusCode.OK, user);
    }

    public async delete(request: RequestModel<UserRequestDeleteInput, UserRequestGeneralParamsInput>): Promise<ResponseModel<undefined>>{
        if(request.params?.id === undefined){
            throw new ApplicationError("no id was provided", KnownApplicationErrors.ID_NOT_PROVIDED, StatusErrorCode.BadRequest);
        }
        const input: UserDeleteInput = {
            searchBy: {
                id: parseInt(request.params.id+'')
            }
        };
        await UserController.instance.userUseCase.delete(input.searchBy.id);
        return UserController.instance.responseHandler.responseNoContent();
    }

    public async search(request: RequestModel<any, any, UserRequestSearchInput>): Promise<ResponseModel<Array<User>>>{
        const inputData: UserSearchInput = {
            searchBy: {
                id: request.query?.id ? parseInt(request.query.id+ '') : undefined,
                firstName: request.query?.firstName,
                lastName: request.query?.lastName,
                secondLastName: request.query?.secondLastName,
                email: request.query?.email
            }
        };
        const users = await UserController.instance.userUseCase.getManyBy(inputData);
        return UserController.instance.responseHandler.response("success", ResponseStatusCode.OK, users);
    }

    public async getById(request: RequestModel<any, UserRequestGeneralParamsInput, UserRequestSearchInput>): Promise<ResponseModel<User>>{
        if(request.params?.id === undefined){
            throw new ApplicationError("no id was provided", KnownApplicationErrors.ID_NOT_PROVIDED, StatusErrorCode.BadRequest);
        }
        const inputData= {
            searchBy: {
                id: parseInt(request.params.id + '')
            }
        };
        const user = await UserController.instance.userUseCase.getById(inputData);
        if(!user){
            throw new ApplicationError("no user with given id was found", KnownApplicationErrors.NOT_FOUND, StatusErrorCode.NotFound);
        }
        return UserController.instance.responseHandler.response("success", ResponseStatusCode.OK, user);
    }

}