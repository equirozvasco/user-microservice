import {FieldType} from "../../../entities/models/global/contants.entity";
import {ApplicationError} from "@smartsoft/backend-utils/dist/errors/application-error.helper";
import {KnownApplicationErrors, StatusErrorCode} from "@smartsoft/backend-utils/dist/entities/errors/errors.entity";

/**
 * checks whether a given value corresponds to a given type (if validation is successful, it will return the casted value)
 * @param value the value to be validated
 * @param type the type it should be validated against
 * @param validValues validValues (required if type is enum)
 * @param allowNull
 */
export const validateFieldType = (value: any, type: FieldType, validValues?: string | Array<string>, allowNull?: boolean): {validationResult: boolean, result?: any} => {
    const enumValues =  validValues ? (Array.isArray(validValues) ? validValues : validValues.split(',') ) : undefined;
    const valueType = typeof value;
    let validationResult: boolean;
    if(allowNull){
        const isNull = value === null || value === "null";
        if(isNull){
            return {
                validationResult: true,
                result: null
            }
        }
    }
    switch (type) {
        case FieldType.enum:
            if(valueType !== "string"){
                throw new ApplicationError("no valid value type provided for 'fieldTypeValidation'", KnownApplicationErrors.INVALID_INPUT, StatusErrorCode.BadRequest);
            }
            if(!enumValues){
                throw new ApplicationError("no validValues provided for 'fieldTypeValidation'", KnownApplicationErrors.INVALID_INPUT, StatusErrorCode.BadRequest);
            }
            validationResult = enumValues.includes(value);
            break;
        case FieldType.string:
            validationResult = valueType === "string";
            break;
        case FieldType.float:
            value = valueType === "string" ? parseFloat(value) : value;
            validationResult = !isNaN(value);
            break;
        case FieldType.integer:
            value = valueType === "string" ? parseInt(value) : value;
            validationResult = !isNaN(value) && Number.isInteger(value);
            break;
        case FieldType.json:
            value = valueType === "string" ? JSON.parse(value) : value;
            validationResult = typeof value === "object";
            break;
        default:
            throw new ApplicationError("no valid type provided for 'fieldTypeValidation'", KnownApplicationErrors.INVALID_INPUT, StatusErrorCode.BadRequest);
    }
    return {validationResult, result: validationResult ? value : undefined};
}