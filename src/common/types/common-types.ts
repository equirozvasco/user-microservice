export enum InputType {
    CREATE,
    UPDATE,
    SEARCH,
    DELETE,
    ACTION
}
