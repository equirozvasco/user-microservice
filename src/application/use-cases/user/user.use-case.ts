import {IUserUseCase} from "../../../entities/use-cases/user/user.use-case.entity";
import {User, UserCreditInfo} from "../../../entities/models/user/user.model.entity"

import {
    UserCreateInput,
    UserSearchCreditInput,
    UserSearchInput,
    UserUpdateInput
} from "../../../entities/schemas/types/user";
import {
    UserCreateInputSchema,
    UserSearchInputSchema,
    UserUpdateInputSchema
} from "../../../entities/schemas/definitions/user/user.schema.entity";
import {ValidationResult} from "joi";
import {IUserRepository} from "../../ports/repositories/user/user.repository.port";
import {KnownApplicationErrors, StatusErrorCode} from "@smartsoft/backend-utils/dist/entities/errors/errors.entity";
import {ApplicationError} from "@smartsoft/backend-utils/dist/errors/application-error.helper";
import {InputType} from "../../../common/types/common-types";

export class UserUseCase implements IUserUseCase{
    constructor(
        private readonly userRepository: IUserRepository
    ) {
    }
    create(input: UserCreateInput): Promise<User>
    create(input: UserCreateInput, transactionManager: any): Promise<User>;
    async create(input: UserCreateInput, transactionManager?: any): Promise<User>{
        this.validateInput(input, InputType.CREATE);
        const operationTransaction = async (manager: any): Promise<User> => {
            const user = await this.userRepository.create(input, manager);
            return user;
        };
        if (transactionManager){
            return operationTransaction(transactionManager);
        }else{
            return this.userRepository.transaction(operationTransaction);
        }
    }

    delete(id: number): Promise<void>;
    delete(id: number, transactionManager: any): Promise<void>;
    async delete(id: number, transactionManager?: any): Promise<void>{
        if (isNaN(id)){
            throw new ApplicationError('invalid id provided', KnownApplicationErrors.INVALID_ID, StatusErrorCode.BadRequest);
        }
        const existing = await this.userRepository.findById({searchBy:{id}}, transactionManager);
        if (!existing){
            throw new ApplicationError("no record was found for given id", KnownApplicationErrors.NOT_FOUND, StatusErrorCode.NotFound);
        }
        return this.userRepository.delete({searchBy:{id}}, transactionManager);
    }

    validateCredit(input: UserSearchCreditInput): Promise<UserCreditInfo>;
    validateCredit(input: UserSearchCreditInput, transactionManager?: any): Promise<UserCreditInfo>;
    async validateCredit(input: UserSearchCreditInput, transactionManager?: any): Promise<UserCreditInfo>{
        this.validateInput(input, InputType.SEARCH);
        const existing = await this.userRepository.findById({searchBy: {id: input.searchBy.id}});
        if(!existing){
            throw new ApplicationError("no user with given id was found", KnownApplicationErrors.NOT_FOUND, StatusErrorCode.NotFound);
        }
        if (existing.credit<input.data.credit){
            throw new ApplicationError("insufficient credit", KnownApplicationErrors.INVALID_OPERATION, StatusErrorCode.BadRequest);
        }
        const operationTransaction = async (manager: any): Promise<UserCreditInfo> => {
            let creditUpdate = existing.credit - input.data.credit;
            input.data.credit = creditUpdate;
            await this.update(input,manager);
            const userCreditInfo: UserCreditInfo = {currentCredit: creditUpdate, active: existing.active}
            return userCreditInfo;
        };
        if(transactionManager){
            return operationTransaction(transactionManager);
        }else{
            return this.userRepository.transaction(operationTransaction);
        }
    }

    getById(input: Omit<UserSearchInput, "searchBy"> & { searchBy: { id: number } }): Promise<User | undefined>;
    getById(input: Omit<UserSearchInput, "searchBy"> & { searchBy: { id: number } }, transactionManager: any): Promise<User | undefined>;
    getById(input: Omit<UserSearchInput, "searchBy"> & { searchBy: { id: number } }, transactionManager?: any): Promise<User | undefined> {
        this.validateInput(input, InputType.SEARCH);
        return this.userRepository.findById(input, transactionManager);
    }

    getManyBy(input: UserSearchInput): Promise<Array<User>> {
        this.validateInput(input, InputType.SEARCH);
        return this.userRepository.findAllBy(input);
    }

    getOneBy(input: UserSearchInput): Promise<User | undefined> {
        this.validateInput(input, InputType.SEARCH);
        return this.userRepository.findOneBy(input);
    }

    update(input: UserUpdateInput): Promise<User>;
    update(input: UserUpdateInput, transactionManager: any): Promise<User>;
    async update(input: UserUpdateInput, transactionManager?: any): Promise<User> {
        const existing = await this.userRepository.findById({searchBy: {id: input.searchBy.id}}, transactionManager);
        if(!existing){
            throw new ApplicationError("no user with given id was found", KnownApplicationErrors.NOT_FOUND, StatusErrorCode.NotFound);
        }
        const operationTransaction = async (manager: any): Promise<User> => {
            const user = await this.userRepository.update(input, manager);
            return user;
        }
        if(transactionManager){
            return operationTransaction(transactionManager);
        }else{
            return this.userRepository.transaction(operationTransaction);
        }
    }

    validateInput(input: any, type: InputType){
        let result: ValidationResult | undefined;
        switch (type) {
            case InputType.CREATE:
                result = UserCreateInputSchema.validate(input);
                break;
            case InputType.UPDATE:
                result = UserUpdateInputSchema.validate(input);
                break;
            case InputType.SEARCH:
                result = UserSearchInputSchema.validate(input);
                break;
            default:
                throw new ApplicationError("invalid input type", KnownApplicationErrors.INVALID_INPUT_TYPE, StatusErrorCode.BadRequest);
        }
        if(result.error){
            throw new ApplicationError(result.error.message, KnownApplicationErrors.INVALID_INPUT, StatusErrorCode.BadRequest);
        }
    }
}

