import {User, UserCreditInfo} from "../../../../entities/models/user/user.model.entity";
import {
    UserSearchCreditInput,
    UserUpdateInput,
    UserSearchInput,
    UserCreateInput
} from "../../../../entities/schemas/types/user"

import {IRepository} from "../repository.port";

export type UserDataCreateInput = UserCreateInput;
export type UserDataUpdateInput = UserUpdateInput;

export interface IUserRepository extends IRepository{
    create(input: UserDataCreateInput): Promise<User>;
    create<T>(input: UserDataCreateInput, transactionManager: T): Promise<User>;
    update(input: UserDataUpdateInput): Promise<User>;
    update<T>(input: UserDataUpdateInput, transactionManager: T): Promise<User>;
    findById(input: Omit<UserSearchInput, "searchBy"> & { searchBy: { id: number } }): Promise<User|undefined>;
    findById<T>(input: Omit<UserSearchInput, "searchBy"> & { searchBy: { id: number } }, transactionManager?: T): Promise<User|undefined>;
    findOneBy(input: UserSearchInput): Promise<User|undefined>;
    findAllBy(input: UserSearchInput): Promise<Array<User>>;
    delete(input: Omit<UserSearchInput, "searchBy"> & { searchBy: { id: number } }): Promise<void>;
    delete<T>(input: Omit<UserSearchInput, "searchBy"> & { searchBy: { id: number } }, transactionManager: T): Promise<void>;
}