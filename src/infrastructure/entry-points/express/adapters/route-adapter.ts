
import { Request, Response, NextFunction } from 'express';
import {ControllerAction} from "@smartsoft/backend-utils/dist/ports/controllers/controller.port";
import {CustomErrorLevel} from "@smartsoft/backend-utils/dist/entities/errors/errors.entity";

export function routeAdapter (action: ControllerAction){
    return async (req: Request, res: Response, next: NextFunction)=>{
        try {
            const response = await action({
                body: req.body,
                headers: {...req.headers},
                params: req.params,
                query: req.query
            });
            res.status(response.statusCode);
            return response.noContentResponse ? res.send() :  res.json({
                status: response.status,
                data: response.data
            });
        }catch (err: any){
            console.log('[ERROR]', err);
            if("_errorCode" in err){
                return res.status(err._statusCode || err._status).json({
                    status: err._statusMessage || (err._statusCode || err._status) + '',
                    errorCode: err._errorCode,
                    error: err._level !== CustomErrorLevel.APPLICATION ? undefined : err._rawErrorMessage
                });
            }else{
                return res.status(500).json({
                    status: "Server Error",
                    error: err.message
                });
            }
        }
    };
}