import { App } from './app';
import {APP_VARIABLES} from "../../../../common/config/app-variables.config";

const port = APP_VARIABLES.APP_REST_PORT;

export function startApp(preventListen = false){
    const app = App();
    if(!preventListen){
        app.listen(port, ()=>{
            console.log('App started on port '+ port);
        });
    }
    return app;
}
