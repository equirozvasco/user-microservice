import { Router } from 'express';
import {userRoutes} from "./user/user.route";


const V1Routes = ()=>{
    const router = Router();
    router.use('/users', userRoutes());
    return router;
};
export {
    V1Routes
};