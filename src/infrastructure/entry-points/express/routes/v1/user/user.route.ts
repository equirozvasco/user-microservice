import {Router} from "express";
import {routeAdapter} from "../../../adapters/route-adapter";
import {userFactory} from "../../../../../../main/factories/user/user.factory";

const userRoutes = ()=>{
    const router = Router({mergeParams: true});
    const {userController} = userFactory();

    router.route('/')
        .get(routeAdapter(userController.search))
        .post(routeAdapter(userController.create));

    router.route(('/:id'))
        .get(routeAdapter(userController.getById))
        .patch(routeAdapter(userController.update))
        .delete(routeAdapter(userController.delete));

    return router;
}

export {
    userRoutes
};