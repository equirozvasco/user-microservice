import {GeneralRepository} from "../general.repository";
import {IUserRepository, UserDataCreateInput,UserDataUpdateInput} from "../../../../../application/ports/repositories/user/user.repository.port";
import {Connection, EntityManager, Repository, SelectQueryBuilder} from "typeorm";
import {UserEntity} from "../../entities/user/user.entity";
import {User} from "../../../../../entities/models/user/user.model.entity";
import {UserSearchInput} from "../../../../../entities/schemas/types/user";
import {DatabaseError} from "@smartsoft/backend-utils/dist/errors/database-error.helper";
import {KnownApplicationErrors, StatusErrorCode} from "@smartsoft/backend-utils/dist/entities/errors/errors.entity";
import {userEntityToDomain} from "../common/transform-data-to-domain.helper";

export class UserRepository extends GeneralRepository implements IUserRepository {
    private readonly repository: Repository<UserEntity>;

    constructor(connection: Connection) {
        super(connection);
        this.repository = connection.getRepository(UserEntity);
    }
    create(input: UserDataCreateInput): Promise<User>;
    create<T>(input: UserDataCreateInput, transactionManager: T): Promise<User>;
    async create(input: UserDataCreateInput, transactionManager?: EntityManager): Promise<User> {
        const user = new UserEntity(
            input.data.firstName,
            input.data.secondName || null,
            input.data.lastName || null,
            input.data.secondLastName || null,
            input.data.user,
            input.data.password,
            input.data.email,
            input.data.rol,
            input.data.credit
        );
        if(transactionManager){
            await transactionManager.save(user);
        }else{
            await this.repository.save(user);
        }
        return userEntityToDomain(user);
    }

    delete(input: Omit<UserSearchInput, "searchBy"> & { searchBy: { id: number } }): Promise<void>;
    delete<T>(input: Omit<UserSearchInput, "searchBy"> & { searchBy: { id: number } }, transactionManager: T): Promise<void>;
    async delete(input: Omit<UserSearchInput, "searchBy"> & { searchBy: { id: number } }, transactionManager?: EntityManager): Promise<void> {
        const user = await this.repository.findOne({where: {id: input.searchBy.id}});
        if(!user){
            throw new DatabaseError("user with given id was not found", KnownApplicationErrors.NOT_FOUND, StatusErrorCode.NotFound);
        }
        if(transactionManager){
            await transactionManager.softRemove(user);
        }else{
            await this.repository.softRemove(user);
        }
    }
    async findAllBy(input: UserSearchInput): Promise<Array<User>> {
        const requesters = await this.generateFilterQuery(input).getMany();
        return requesters.map(el=>userEntityToDomain(el));
    }

    findById(input: Omit<UserSearchInput, "searchBy"> & { searchBy: { id: number } }): Promise<User | undefined>;
    findById<T>(input: Omit<UserSearchInput, "searchBy"> & { searchBy: { id: number } }, transactionManager?: T): Promise<User | undefined>;
    async findById(input: Omit<UserSearchInput, "searchBy"> & { searchBy: { id: number } }, transactionManager?: EntityManager): Promise<User | undefined> {
        const user = await this.generateFilterQuery(input, transactionManager).getOne();
        return user ? userEntityToDomain(user) : undefined;
    }

    async findOneBy(input: UserSearchInput): Promise<User | undefined> {
        const user = await this.generateFilterQuery(input).getOne();
        return user ? userEntityToDomain(user) : undefined;
    }

    update(input: UserDataUpdateInput): Promise<User>;
    update<T>(input: UserDataUpdateInput, transactionManager: T): Promise<User>;
    async update(input: UserDataUpdateInput, transactionManager?: EntityManager): Promise<User> {
        const user = await this.repository.findOne({where: {id: input.searchBy.id}});
        if(!user){
            throw new DatabaseError("user with given id was not found", KnownApplicationErrors.NOT_FOUND, StatusErrorCode.NotFound);
        }
        if(input.data.firstName !== undefined){
            user.firstName = input.data.firstName;
        }
        if(input.data.secondName !== undefined){
            user.secondName = input.data.secondName;
        }
        if(input.data.lastName !== undefined){
            user.lastName = input.data.lastName;
        }
        if(input.data.secondLastName !== undefined){
            user.secondLastName = input.data.secondLastName;
        }
        if(input.data.email !== undefined){
            user.email = input.data.email;
        }
        if(input.data.user !== undefined){
            user.user = input.data.user;
        }
        if(input.data.credit !== undefined){
            user.credit = input.data.credit;
        }
        if(input.data.rol !== undefined){
            user.rol = input.data.rol;
        }
        if(transactionManager){
            await transactionManager.save(user);
        }else{
            await this.repository.save(user);
        }
        return userEntityToDomain(user);
    }

    private generateFilterQuery(input: UserSearchInput, transactionManager?: EntityManager): SelectQueryBuilder<UserEntity> {
        let sq = transactionManager?.createQueryBuilder(UserEntity, "user") || this.repository.createQueryBuilder("user");
        const query: {where: Array<string>, params: {[att: string]: any}} = {params: {}, where: []};

        if(input.searchBy?.id !== undefined){
            query.where.push("requester.id = :id");
            query.params["id"] = input.searchBy.id;
        }

        if(input.searchBy?.firstName !== undefined){
            query.where.push('requester."firstName" = :firstName');
            query.params["firstName"] = input.searchBy.firstName;
        }

        if(input.searchBy?.lastName !== undefined){
            query.where.push('requester."lastName" = :lastName');
            query.params["lastName"] = input.searchBy.lastName;
        }

        if(input.searchBy?.secondLastName !== undefined){
            query.where.push('user."secondLastName" = :secondLastName');
            query.params["secondLastName"] = input.searchBy.secondLastName;
        }
        if(input.searchBy?.user !== undefined){
            query.where.push('user."user" = :user');
            query.params["user"] = input.searchBy.user;
        }

        if(input.searchBy?.email !== undefined){
            query.where.push('user."email" = :email');
            query.params["email"] = input.searchBy.email;
        }

        if(query.where.length > 0){
            sq = sq.where(query.where.join(" and "), query.params);
        }
        return sq;
    }


}