import {FieldType} from "../../../../../entities/models/global/contants.entity";
import {UserEntity} from '../../entities/user/user.entity';
import {User} from '../../../../../entities/models/user/user.model.entity';

export function userEntityToDomain(entity: UserEntity): User{
    return {
        id: entity.id,
        firstName: entity.firstName,
        secondName: entity.secondName,
        lastName: entity.lastName,
        secondLastName: entity.secondLastName,
        user: entity.user,
        password: entity.password,
        email: entity.email,
        rol: entity.rol,
        credit: entity.credit,
        active: entity.active,
        createAt: entity.createdAt,
        updateAt: entity.updatedAt,
        deleteAt: entity.deletedAt
    }
}
