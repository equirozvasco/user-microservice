import {Connection, ConnectionOptions, createConnection, getConnection} from 'typeorm';

let connection: Connection;

import {connection as postgresConnectionConfig} from "./connection.postgres";

export async function connectToDb(connectionOptions?: ConnectionOptions){
    try{
        connection = await createConnection(connectionOptions || postgresConnectionConfig);
        console.log('DB Connection established');
    }catch (err){
        console.log('DB Connection failed', err);
        throw err;
    }
}

export async function closeDbConnection(){
    try{
        if(connection){
            await connection.close();
            console.log('DB connection closed');
        }
    }catch(err){
        console.log('DB connection could not be closed', err);
        throw err;
    }
}

export function getDbConnection(forceGet: boolean = false){
    if (forceGet){
        connection = getConnection();
    }
    return connection;
}