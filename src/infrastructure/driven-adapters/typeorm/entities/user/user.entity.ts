import {
    Column,
    CreateDateColumn,
    DeleteDateColumn, Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";

@Entity()
export class UserEntity{
    @PrimaryGeneratedColumn()
    id!: number;
    @Column({type: "varchar", nullable: false})
    firstName: string;
    @Column({type: "varchar", nullable: true})
    secondName: string | null;
    @Column({type: "varchar", nullable: true})
    lastName: string | null;
    @Column({type: "varchar", nullable: true})
    secondLastName: string | null;
    @Column({type: "varchar", nullable: false})
    user: string;
    @Column({type: "varchar", nullable: false})
    password: string;
    @Column({type: "varchar", nullable: false})
    email: string;
    @Column({type: "varchar", nullable: false})
    rol: string;
    @Column({type: "number", nullable: false})
    credit: number;
    @Column({type: "boolean", nullable: false, default: true})
    active: boolean;
    @CreateDateColumn()
    createdAt!: Date;
    @UpdateDateColumn()
    updatedAt!: Date;
    @DeleteDateColumn()
    deletedAt!: Date | null;
    constructor(
        firstName: string,
        secondName: string | null,
        lastName: string | null,
        secondLastName: string | null,
        user: string,
        password: string,
        email: string,
        rol: string,
        credit: number,
    ) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.secondLastName = secondLastName;
        this.user = user;
        this.password = password;
        this.email = email;
        this.rol = rol;
        this.credit = credit;
        this.active = true;
    }
}